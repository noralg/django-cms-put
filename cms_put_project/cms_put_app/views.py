from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from .models import Contenido
# Create your views here.

form = """
<form action="" method="POST">
    {% csrf_token %}
    <label>Introduce el recurso para guardar:</label>
        <input type="text" name="valor" autocomplete="off" required>
    <button type="submit">Enviar</button>
</form>
"""
def index(request):
    return HttpResponse("<html><body><h1>Bienvenido a cms-put</h1></html></body>")

@csrf_exempt
def get_content(request, llave):
    if request.method == "POST":
        valor = request.POST.get('valor') #guardamos directamente el valor en la DB
        content, created = Contenido.objects.get_or_create(clave=llave)
        content.valor = valor
        content.save()
    elif request.method == "PUT":
        valor = request.body.decode('utf-8')
        # actualizamos el valor existente o, si no existe, creamos uno nuevo
        content, created = Contenido.objects.update_or_create(clave=llave, defaults={'valor':valor})

    try:
        response = Contenido.objects.get(clave=llave).valor
    except Contenido.DoesNotExist:
        response = f'''
            <html>
            <body>
                <h1>No existe contenido para la clave {llave}.</h1>
                <h2>Quieres establecer un valor para la clave?</h2>
                {form}
            </body>
            </html>      
        '''
    return HttpResponse(response)
